#include <QPlainTextEdit>

#include "stasticpanel.h"

#include "studentmodel.h"

StasticPanel::StasticPanel(QWidget *parent) : QWidget(parent),
    m_result(new QPlainTextEdit(this)),
    m_model(nullptr)
{
    m_singleWidgets[0]="Like";
    m_singleWidgets[1]="Like grp";
    m_singleWidgets[2]="Radio grp";
    m_singleWidgets[3]="Heart";
    m_singleWidgets[4]="Slider";

    m_singleMap.insert("Single 'Like' button"             , "Like"      );
    m_singleMap.insert("'Like' and 'Dislike' button group", "Like grp");
    m_singleMap.insert("Radio button groups"              , "Radio grp" );
    m_singleMap.insert("Red heart rating"                 , "Heart"     );
    m_singleMap.insert("Slider rating"                    , "Slider"    );

    m_multipleWidgets[0]="List";
    m_multipleWidgets[1]="Grid";
    m_multipleWidgets[2]="Driving";
    m_multipleWidgets[3]="Two-lv";

    m_multipleMap.insert("List user interface"               , "List"   );
    m_multipleMap.insert("Grid user interface"               , "Grid"   );
    m_multipleMap.insert("Driving simulator user interface"  , "Driving");
    m_multipleMap.insert("Two-level selection user interface", "Two-lv" );

    m_title[MetaGender          ]="MetaGender";
    m_title[MetaAge             ]="MetaAge";
    m_title[MetaKnown           ]="MetaKnows";
    m_title[SingleLike          ]="SingleLike";
    m_title[SingleHelp          ]="SingleHelp";
    m_title[SingleAffect        ]="SingleAffect";
    m_title[MultipleLike        ]="MultipleLike";
    m_title[MultipleHelp        ]="MultipleHelp";
    m_title[MultipleSlow        ]="MultipleSlow";
    m_title[MultipleComplex     ]="MultipleComplex";
    m_title[MultipleFaster      ]="MultipleFaster";
    m_title[MultipleWidget      ]="MultipleWidget";
    m_title[MultipleWidgetAffect]="MultipleWidgetAffect";

    QFont resultFont=m_result->font();
    resultFont.setFamily("Consolas");
    m_result->setFont(resultFont);
}

void StasticPanel::doStatstics()
{
    if(m_model==nullptr)
    {
        return;
    }
    m_results[MetaGender]=QHash<QString, int>();
    m_results[MetaAge]=QHash<QString, int>();
    m_results[MetaKnown]=QHash<QString, int>();

    for(int i=SingleLike; i<SingleCount; ++i)
    {
        //Initial the data.
        for(int j=0; j<5; ++j)
        {
            m_results[i].insert(m_singleWidgets[j], 0);
        }
    }

    for(int i=MultipleLike; i<MultipleWidget; ++i)
    {
        //Initial the data.
        for(int j=0; j<4; ++j)
        {
            m_results[i].insert(m_multipleWidgets[j], 0);
        }
    }
    //Loop to calculate the data.
    int rowCount=m_model->rowCount();
    for(int i=0; i<rowCount; ++i)
    {
        //Get the answer for all the metadata.
        const StudentInfo &student=m_model->studentInfo(i);
        addData(MetaGender, student.gender);
        addData(MetaAge, QString::number(student.age));
        addData(MetaKnown, student.know);
        //Get the single answer.
        const QJsonArray &singleAns=student.single_answer;
        addData(SingleLike, m_singleMap.value(singleAns.at(0).toString()));
        m_resultReasons[SingleLike].append(singleAns.at(1).toString());
        addSingleListData(SingleHelp, singleAns.at(2).toArray());
        m_resultReasons[SingleHelp].append(singleAns.at(3).toString());
        addSingleListData(SingleAffect, singleAns.at(4).toArray());
        m_resultReasons[SingleAffect].append(singleAns.at(5).toString());
        //Get the multiple answer.
        const QJsonArray &multipleAns=student.multiple_answer;
        addData(MultipleLike, m_multipleMap.value(multipleAns.at(0).toString()));
        m_resultReasons[MultipleLike].append(multipleAns.at(1).toString());
        addMultipleListData(MultipleHelp, multipleAns.at(2).toArray());
        m_resultReasons[MultipleHelp].append(multipleAns.at(3).toString());
        addMultipleListData(MultipleSlow, multipleAns.at(4).toArray());
        m_resultReasons[MultipleSlow].append(multipleAns.at(5).toString());
        addData(MultipleComplex, m_multipleMap.value(multipleAns.at(6).toString()));
        m_resultReasons[MultipleComplex].append(multipleAns.at(7).toString());
        addMultipleListData(MultipleFaster, multipleAns.at(8).toArray());
        addData(MultipleWidget, multipleAns.at(9).toString());
        m_resultReasons[MultipleWidget].append(multipleAns.at(10).toString());
        addData(MultipleWidgetAffect, multipleAns.at(11).toString());
        m_resultReasons[MultipleWidgetAffect].append(multipleAns.at(12).toString());
    }
    //Auto show result.
    showResult();
}

void StasticPanel::showResult()
{
    QString resultText="";
    //Loop and show the result.
    for(int i=0; i<QuestionCounts; ++i)
    {
        resultText.append(m_title[i] + ":\n");
        QList<QString> keys=m_results[i].keys();
        //Sort the keys.
        std::sort(keys.begin(), keys.end());
        //Get the longest string length.
        int maxLength=-1, maxValue=-1;
        for(auto j:keys)
        {
            maxLength=qMax(maxLength, j.length());
            maxValue=qMax(maxValue, m_results[i].value(j));
        }
        //Add the result data.
        for(int j=0; j<keys.size(); ++j)
        {
            int countData=m_results[i].value(keys.at(j));
            resultText.append(titleText(keys.at(j), maxLength) + ": " +
                              QString("*").repeated(countData) +
                              QString(" ").repeated(maxValue-countData)+ " " +
                              QString::number(countData) + "\n");
        }
//        //Check result.
//        if(m_resultReasons[i].size()!=0)
//        {
//            resultText.append("Reasons:\n" +
//                              m_resultReasons[i].join('\n') + "\n");
//        }
        resultText.append("\n");
    }
    m_result->setPlainText(resultText);
}

void StasticPanel::showLatex()
{
    QString texText;
    texText = "\\documentclass{article}\n"
              "\\usepackage{pgfplotstable}\n"
              "\n";
    QStringList csvList, documentList;
    //Loop and show the result.
    for(int i=0; i<QuestionCounts; ++i)
    {
        QString csvData, documentData;

        documentData =
                "\\pgfplotstableread[col sep=comma]{graph-" + QString::number(i)
                + ".csv}\\graphcsv\n"
                "    \\begin{tikzpicture}\n"
                "        \\begin{axis}[\n"
                "               xbar,\n"
                "               y=0.5cm, enlarge y limits={true, abs value=0.75},\n"
                "               xmin=0, enlarge x limits={upper, value=0.15},\n"
                "               xlabel=Numbers of People,\n"
                "               xmajorgrids=true,\n"
                "               ytick=data,\n"
                "               yticklabels={%1},\n"
                "               nodes near coords, nodes near coords align=horizontal\n"
                "        ]\n"
                "        \\addplot table [x=b, y=a]\n"
                "        {\\graphcsv};\n"
                "        \\end{axis}\n"
                "    \\end{tikzpicture}\n";

        csvData = "\\begin{filecontents}{graph-" + QString::number(i) +
                ".csv}\n"
                "a,b\n";

        QList<QString> keys=m_results[i].keys();
        //Sort the keys.
        std::sort(keys.begin(), keys.end());
        documentList.append(documentData.arg(keys.join(", ")));
        //Add the result data.
        for(int j=0; j<keys.size(); ++j)
        {
            csvData.append(
                        QString::number(j) + "," +
                        QString::number(m_results[i].value(keys.at(j))) + "\n");
        }
        csvData.append("\\end{filecontents}\n");
        csvList.append(csvData);
//        resultText.append("\n");
    }
    //Set the text.
    texText.append(csvList.join('\n') + "\n\\begin{document}\n    " +
                   documentList.join("\n    ") +
                   "\\end{document}");
    m_result->setPlainText(texText);
}

void StasticPanel::setModel(StudentModel *model)
{
    m_model=model;
}

void StasticPanel::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    m_result->resize(size());
}

void StasticPanel::addData(int index, const QString &data)
{
    if(m_results[index].contains(data))
    {
        m_results[index].insert(data, m_results[index].value(data)+1);
    }
    else
    {
        m_results[index].insert(data, 1);
    }
}

void StasticPanel::addSingleListData(int index, const QJsonArray &data)
{
    for(int i=0; i<data.size(); ++i)
    {
        addData(index, m_singleWidgets[data.at(i).toInt()]);
    }
}

void StasticPanel::addMultipleListData(int index, const QJsonArray &data)
{
    for(int i=0; i<data.size(); ++i)
    {
        addData(index, m_multipleWidgets[data.at(i).toInt()]);
    }
}

QString StasticPanel::titleText(const QString &content, int length)
{
    if(content.size() < length)
    {
        return QString(" ").repeated(length-content.size()) + content;
    }
    else
    {
        return content;
    }
}
