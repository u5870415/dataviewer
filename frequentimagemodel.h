/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#ifndef FREQUENTIMAGEMODEL_H
#define FREQUENTIMAGEMODEL_H

#include <QPixmap>

#include <QAbstractListModel>

class FrequentImage;
class FrequentImageModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit FrequentImageModel(QObject *parent = 0);
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QPixmap getImage(int row);

signals:

public slots:
    void addPoints(int index, const QList<QPointF> &pointList);
    void renderAll();

private:
    FrequentImage *m_images[9];
};

#endif // FREQUENTIMAGEMODEL_H
