#ifndef TRACKDISPLAY_H
#define TRACKDISPLAY_H

#include <QTime>

#include <QWidget>

class TrackDisplay : public QWidget
{
    Q_OBJECT
public:
    explicit TrackDisplay(QWidget *parent = 0);
    QPixmap background() const;
    int duration() const;

signals:

public slots:
    void setBackground(const QPixmap &background);
    void setDuration(int duration);
    void setTrack(const QList<QPointF> &track);

protected:
    void mouseReleaseEvent(QMouseEvent *event) override;
    void paintEvent(QPaintEvent *event) override;

private:
    QPointF previousPoint(const QList<QPointF> &list,
                          int currentIndex);
    QTimer *m_anime;
    QPixmap m_trackRender, m_background;
    QList<QPointF> m_track, m_rawTrack;
    QTime m_startTime;
    int m_currentFrame, m_duration;
    bool m_displayAll;
};

#endif // TRACKDISPLAY_H
