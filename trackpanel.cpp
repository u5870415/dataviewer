#include <QBoxLayout>
#include <QItemSelectionModel>
#include <QStringListModel>
#include <QListView>

#include "trackdisplay.h"

#include "trackpanel.h"

#include <QDebug>

TrackPanel::TrackPanel(QWidget *parent) : QWidget(parent),
    m_listModel(new QStringListModel(this)),
    m_trackList(new QListView(this)),
    m_display(new TrackDisplay(this))
{
    m_trackList->setFixedHeight(100);
    m_trackList->setModel(m_listModel);
    connect(m_trackList->selectionModel(),
            &QItemSelectionModel::currentChanged,
            [=](const QModelIndex &index)
    {
        //Get the index.
        int trackIndex=index.row();
        //Set the frame updating.
        m_display->setDuration(m_durations.at(trackIndex));
        //Set the background first.
        m_display->setBackground(m_backgrounds.at(
                                     (m_pointerBackgrounds.at(trackIndex)>>1)));
        //Get the index data.
        m_display->setTrack(m_pointerList.at(trackIndex));
    });
    QBoxLayout *mainLayout=new QBoxLayout(QBoxLayout::TopToBottom, this);
    mainLayout->addWidget(m_trackList);
    mainLayout->addWidget(m_display);
    setLayout(mainLayout);
}

void TrackPanel::setTracks(const QList<qint64> &durations,
                           const QStringList &durationList,
                           const QList<QList<QPointF> > &pointerList,
                           const QList<int> &pointerBackgrounds)
{
    //Save durations.
    m_durations=durations;
    //Save the background list.
    m_pointerBackgrounds = pointerBackgrounds;
    //Save the list.
    m_pointerList = pointerList;
    //Get the list size.
    QStringList indexList;
    for(int i=0; i<pointerList.size(); ++i)
    {
        //Append the data.
        indexList.append(QString::number(i) + " - " + durationList.at(i));
    }
    //Set the model.
    m_listModel->setStringList(indexList);
}

QList<QPixmap> TrackPanel::backgrounds() const
{
    return m_backgrounds;
}

void TrackPanel::setBackgrounds(const QList<QPixmap> &backgrounds)
{
    m_backgrounds = backgrounds;
}
