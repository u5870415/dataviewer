QT += core gui widgets sql
CONFIG += c++11

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    databaseloader.cpp \
    studentmodel.cpp \
    metadatapanel.cpp \
    singleuipanel.cpp \
    trackpanel.cpp \
    trackdisplay.cpp \
    loaddialog.cpp \
    stasticpanel.cpp \
    frequentimage.cpp \
    frequentimagemodel.cpp \
    frequentimagepanel.cpp \
    doublelayerviewer.cpp

HEADERS += \
    mainwindow.h \
    databaseloader.h \
    studentmodel.h \
    util.h \
    metadatapanel.h \
    singleuipanel.h \
    trackpanel.h \
    trackdisplay.h \
    loaddialog.h \
    stasticpanel.h \
    frequentimage.h \
    frequentimagemodel.h \
    frequentimagepanel.h \
    doublelayerviewer.h

RESOURCES += \
    res.qrc
