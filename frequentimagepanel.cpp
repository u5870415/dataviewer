/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#include <QListView>
#include <QBoxLayout>
#include <QItemSelectionModel>

#include "doublelayerviewer.h"
#include "frequentimagemodel.h"

#include "frequentimagepanel.h"

FrequentImagePanel::FrequentImagePanel(QWidget *parent) : QWidget(parent),
    m_listView(new QListView(this)),
    m_imageViewer(new DoubleLayerViewer(this))
{
    m_listView->setFixedHeight(100);

    m_backgrounds << QPixmap("://images/Single_Yes.png")
                  << QPixmap("://images/Single_Yes_No.png")
                  << QPixmap("://images/Single_Radio.png")
                  << QPixmap("://images/Single_Heart.png")
                  << QPixmap("://images/Single_Slider.png")
                  << QPixmap("://images/Multiple_list.png")
                  << QPixmap("://images/Multiple_grid.png")
                  << QPixmap("://images/Multiple_drive.png")
                  << QPixmap("://images/Multiple_double.png");

    QBoxLayout *mainLayout=new QBoxLayout(QBoxLayout::TopToBottom,this);
    setLayout(mainLayout);
    mainLayout->addWidget(m_listView);
    mainLayout->addWidget(m_imageViewer);
}

void FrequentImagePanel::setModel(FrequentImageModel *model)
{
    m_model=model;
    m_listView->setModel(model);
    connect(m_listView->selectionModel(), &QItemSelectionModel::currentChanged,
            [=](const QModelIndex &index)
    {
        m_imageViewer->setBackground(m_backgrounds.at(index.row()));
        m_imageViewer->setForeground(m_model->getImage(index.row()));
    });
}
