#include <QPainter>
#include <QTimer>

#include "trackdisplay.h"

#include <QDebug>

#define PointSize 5

TrackDisplay::TrackDisplay(QWidget *parent) : QWidget(parent),
    m_anime(new QTimer(this)),
    m_currentFrame(0),
    m_duration(0),
    m_displayAll(true)
{
    setAutoFillBackground(true);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    m_anime->setInterval(30);
    connect(m_anime, &QTimer::timeout,
            [=]
    {
        //Check the frame.
        if(m_currentFrame==m_rawTrack.size()-1)
        {
            m_anime->stop();
            m_displayAll=true;
            update();
            return;
        }
        //Increase the current frame.
        ++m_currentFrame;
        update();
    });
}

void TrackDisplay::setTrack(const QList<QPointF> &track)
{
    //Clear the track.
    m_track=QList<QPointF>();
    m_rawTrack=QList<QPointF>();
    //Initial the painter.
    QPainter painter(&m_trackRender);
    painter.setRenderHints(QPainter::Antialiasing |
                           QPainter::TextAntialiasing |
                           QPainter::SmoothPixmapTransform, true);
    //Update the interval.
    m_anime->setInterval(1000.0/((qreal)track.size()*1000.0/(qreal)m_duration));
    //Save the track.
    for(int i=0; i<track.size(); ++i)
    {
        //Get the point.
        QPointF point=track.at(i);
        //Check the point.
        if(point==QPointF(0,0))
        {
            m_rawTrack.append(point);
            continue;
        }
        //Draw the point.
        painter.drawEllipse(point, PointSize, PointSize);
        //Check i.
        if(i > 0)
        {
            //Get the previous point.
            QPointF previous=previousPoint(track, i);
            if(!previous.isNull())
            {
                //Draw the linking line.
                painter.drawLine(previous, point);
            }
        }
        QPointF cpoint=QPointF(point.x() / 1920.0,
                               point.y() / 1080.0);
        //Append data to new track.
        m_track.append(cpoint);
        m_rawTrack.append(cpoint);
    }
    update();
}

void TrackDisplay::mouseReleaseEvent(QMouseEvent *event)
{
    QWidget::mouseReleaseEvent(event);
    // CHeck the running state.
    if (m_displayAll)
    {
        //Reset frame.
        m_currentFrame=0;
        m_startTime=QTime::currentTime();
        //Start animation.
        m_anime->start();
        m_displayAll=false;
        update();
    }
    else
    {
        //Stop animation.
        m_anime->stop();
        m_displayAll=true;
        update();
    }
}

void TrackDisplay::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent(event);
    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing |
                           QPainter::SmoothPixmapTransform, true);
    if(!m_trackRender.isNull())
    {
        //Check running state.
        if(m_displayAll)
        {
            QPixmap scaledImage=m_trackRender.scaled(
                        size(),
                        Qt::KeepAspectRatio,
                        Qt::SmoothTransformation);
            painter.drawPixmap((width()-scaledImage.width())>>1,
                               (height()-scaledImage.height())>>1,
                               scaledImage);
        }
        else
        {
            //Draw the background.
            QPixmap scaledImage=m_background.scaled(
                        size(),
                        Qt::KeepAspectRatio,
                        Qt::SmoothTransformation);
            int imageX = (width()-scaledImage.width())>>1,
                imageY = (height()-scaledImage.height())>>1;
            painter.drawPixmap(imageX, imageY, scaledImage);
            if(m_currentFrame < m_rawTrack.size() && m_currentFrame > -1)
            {
                //Calculate the current post.
                QPointF watchingPos=m_rawTrack.at(m_currentFrame);
                //Scale the pos.
                watchingPos=QPointF(watchingPos.x() * scaledImage.width() + imageX,
                                    watchingPos.y() * scaledImage.height() + imageY);
                // Draw the pos.
                painter.setBrush(QColor(0,0,0,150));
                painter.setPen(QColor(0,0,0,0));
                painter.drawEllipse(watchingPos, 10, 10);
            }
        }
    }
}

QPointF TrackDisplay::previousPoint(const QList<QPointF> &list,
                                    int currentIndex)
{
    for (int i=currentIndex-1; i>-1; --i)
    {
        if(list.at(i)!=QPointF(0.0, 0.0))
        {
            return list.at(i);
        }
    }
    return QPointF();
}

int TrackDisplay::duration() const
{
    return m_duration;
}

void TrackDisplay::setDuration(int duration)
{
    m_duration = duration;
}

QPixmap TrackDisplay::background() const
{
    return m_trackRender;
}

void TrackDisplay::setBackground(const QPixmap &background)
{
    //Stop the animation.
    m_anime->stop();
    m_displayAll=true;
    //Save the new background.
    m_trackRender = background;
    m_background = background;
    //Update.
    update();
}
