/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#include "frequentimage.h"

#include "frequentimagemodel.h"

FrequentImageModel::FrequentImageModel(QObject *parent) :
    QAbstractListModel(parent)
{
    for(int i=0; i<9; ++i)
    {
        m_images[i]=new FrequentImage(this);
    }
}

int FrequentImageModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 9;
}

QVariant FrequentImageModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(role==Qt::DisplayRole)
        return QString::number(index.row());
    return QVariant();
}

QPixmap FrequentImageModel::getImage(int row)
{
    return QPixmap::fromImage(m_images[row]->image());
}

void FrequentImageModel::addPoints(int index, const QList<QPointF> &pointList)
{
    m_images[index]->addPoints(pointList);
}

void FrequentImageModel::renderAll()
{
    for(int i=0; i<9; ++i)
    {
        m_images[i]->renderingImage();
    }
}
