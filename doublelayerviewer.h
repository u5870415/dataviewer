/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#ifndef DOUBLELAYERVIEWER_H
#define DOUBLELAYERVIEWER_H

#include <QPixmap>

#include <QWidget>

class DoubleLayerViewer : public QWidget
{
    Q_OBJECT
public:
    explicit DoubleLayerViewer(QWidget *parent = 0);

    QPixmap foreground() const;
    void setForeground(const QPixmap &foreground);

    QPixmap background() const;
    void setBackground(const QPixmap &background);

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    inline void updateForeground();
    inline void updateBackground();
    QPixmap m_foreground, m_background;
    QPixmap m_scaledForeground, m_scaledBackground;
    int m_imageX, m_imageY;
};

#endif // DOUBLELAYERVIEWER_H
