#include <QCheckBox>
#include <QRadioButton>

#include <QBoxLayout>

#include "singleuipanel.h"

SingleUiPanel::SingleUiPanel(const QStringList &items,
                             bool isMultiple,
                             QWidget *parent) : QWidget(parent)
{
    setContentsMargins(0,0,0,0);

    QBoxLayout *mainLayout=new QBoxLayout(QBoxLayout::LeftToRight, this);
    if(!isMultiple)
    {
        for (int i=0; i<items.size(); ++i)
        {
            QRadioButton *button=new QRadioButton(items.at(i),this);
            m_selections.append(button);
            mainLayout->addWidget(button);
        }
    }
    else
    {
        for (int i=0; i<items.size(); ++i)
        {
            QCheckBox *button=new QCheckBox(items.at(i), this);
            m_selections.append(button);
            mainLayout->addWidget(button);
        }
    }
    setLayout(mainLayout);
}

void SingleUiPanel::setData(const QString &singleChoice)
{
    for (int i=0; i<m_selections.size(); ++i)
    {
        m_selections.at(i)->setChecked(
                    m_selections.at(i)->text()==singleChoice);
    }
}

void SingleUiPanel::setData(const QJsonArray &multipleChoice)
{
    for (int i=0; i<m_selections.size(); ++i)
    {
        m_selections.at(i)->setChecked(false);
    }
    for(auto i:multipleChoice)
    {
        m_selections.at(i.toInt())->setChecked(true);
    }
}
