#ifndef TRACKPANEL_H
#define TRACKPANEL_H

#include <QWidget>

class QStringListModel;
class QListView;
class TrackDisplay;
class TrackPanel : public QWidget
{
    Q_OBJECT
public:
    explicit TrackPanel(QWidget *parent = 0);

    QList<QPixmap> backgrounds() const;
    void setBackgrounds(const QList<QPixmap> &backgrounds);

signals:

public slots:
    void setTracks(const QList<qint64> &durations,
                   const QStringList &durationList,
                   const QList<QList<QPointF>> &pointerList,
                   const QList<int> &pointerBackgrounds);

private:
    QStringListModel *m_listModel;
    QList<qint64> m_durations;
    QList<QList<QPointF> > m_pointerList;
    QList<int> m_pointerBackgrounds;
    QList<QPixmap> m_backgrounds;
    QListView *m_trackList;
    TrackDisplay *m_display;
};

#endif // TRACKPANEL_H
