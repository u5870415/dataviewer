#ifndef STASTICPANEL_H
#define STASTICPANEL_H

#include <QWidget>

class StudentModel;
class QPlainTextEdit;
class StasticPanel : public QWidget
{
    Q_OBJECT
public:
    explicit StasticPanel(QWidget *parent = 0);
    void doStatstics();
    void showResult();
    void showLatex();

signals:

public slots:
    void setModel(StudentModel *model);

protected:
    void resizeEvent(QResizeEvent *event) override;

private:
    enum StatsticQuestions
    {
        MetaGender,
        MetaAge,
        MetaKnown,
        SingleLike,
        SingleHelp,
        SingleAffect,
        MultipleLike,
        MultipleHelp,
        MultipleSlow,
        MultipleComplex,
        MultipleFaster,
        MultipleWidget,
        MultipleWidgetAffect,
        QuestionCounts,
        SingleCount = MultipleLike,
        MetaCount = SingleLike
    };
    void addData(int index, const QString &data);
    void addSingleListData(int index, const QJsonArray &data);
    void addMultipleListData(int index, const QJsonArray &data);
    QString titleText(const QString &content, int length);

    QString m_singleWidgets[5], m_multipleWidgets[4];
    QString m_title[QuestionCounts];
    QPlainTextEdit *m_result;
    QStringList m_resultReasons[QuestionCounts];
    QHash<QString, int> m_results[QuestionCounts];
    QHash<QString, QString> m_singleMap, m_multipleMap;
    StudentModel *m_model;
};

#endif // STASTICPANEL_H
