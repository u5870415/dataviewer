#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QThread>
#include <QMainWindow>

class FrequentImagePanel;
class FrequentImageModel;
class MetadataPanel;
class StasticPanel;
class TrackPanel;
class DatabaseLoader;
class StudentModel;
class LoadDialog;
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void requireLoadDatabase(const QString &databasePath);

public slots:

private:
    QList<qint64> durationsTotal(const QList<qint64> &duration_first,
                                 const QList<qint64> &duration_second);
    QStringList durationList(const QList<qint64> &durations);
    QStringList durationList(const QList<qint64> &duration_first,
                             const QList<qint64> &duration_second);
    QList<QList<int>> m_multipleTracks;
    QThread m_loaderThread;
    LoadDialog *m_dialog;
    DatabaseLoader *m_loader;
    StudentModel *m_studentModel;
    MetadataPanel *m_metadataPanel;
    StasticPanel *m_statsticPanel;
    FrequentImageModel *m_frequentModel;
    FrequentImagePanel *m_frequentPanel;
    TrackPanel *m_singleTrackPanel, *m_multipleTrackPanel;
};

#endif // MAINWINDOW_H
