#include "studentmodel.h"

StudentModel::StudentModel(QObject *parent) : QAbstractListModel(parent)
{

}

int StudentModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_studentList.size();
}

QVariant StudentModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
    {
        return QVariant();
    }
    const StudentInfo &student=m_studentList.at(index.row());
    switch(role)
    {
    case Qt::DisplayRole:
    case Qt::EditRole:
        return student.row + " - " + student.uid;
    default:
        return QVariant();
    }
}

void StudentModel::append(const StudentInfo &data)
{
    beginInsertRows(QModelIndex(), m_studentList.size(), m_studentList.size());
    m_studentList.append(data);
    endInsertRows();
}

void StudentModel::reset()
{
    if(m_studentList.isEmpty())
    {
        return;
    }
    beginResetModel();
    m_studentList.clear();
    endResetModel();
}

StudentInfo StudentModel::studentInfo(int row)
{
    return m_studentList.at(row);
}

StudentInfo StudentModel::studentInfo(const QModelIndex &index)
{
    return m_studentList.at(index.row());
}
