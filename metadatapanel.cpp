#include <QLineEdit>
#include <QBoxLayout>
#include <QLabel>
#include <QGroupBox>
#include <QFormLayout>

#include "singleuipanel.h"
#include "metadatapanel.h"

MetadataPanel::MetadataPanel(QWidget *parent) : QWidget(parent)
{
    for(int i=0; i<MetadataInfoCount; ++i)
    {
        m_metaInfos[i]=new QLineEdit(this);
    }

    QStringList singleUiNames;
    singleUiNames << "Single 'Like' button"
                  << "'Like' and 'Dislike' button group"
                  << "Radio button groups"
                  << "Red heart rating"
                  << "Slider rating";
    m_singleChoices[0]=new SingleUiPanel(singleUiNames, false, this);
    m_singleChoices[1]=new SingleUiPanel(singleUiNames, true, this);
    m_singleChoices[2]=new SingleUiPanel(singleUiNames, true, this);

    QStringList multipleUiNames;
    multipleUiNames << "List user interface"
                    << "Grid user interface"
                    << "Driving simulator user interface"
                    << "Two-level selection user interface";
    m_multipleChoices[0]=new SingleUiPanel(multipleUiNames, false, this);
    m_multipleChoices[1]=new SingleUiPanel(multipleUiNames, true, this);
    m_multipleChoices[2]=new SingleUiPanel(multipleUiNames, true, this);
    m_multipleChoices[3]=new SingleUiPanel(multipleUiNames, false, this);
    m_multipleChoices[4]=new SingleUiPanel(multipleUiNames, true, this);

    QStringList multipleWidgets;
    multipleWidgets << "Heart Rating"
            << "Slider";
    m_multiSelectionWidget=new SingleUiPanel(multipleWidgets, false, this);
    QStringList multipleAffect;
    multipleAffect << "Yes" << "No";
    m_multiSelectionAffect=new SingleUiPanel(multipleAffect, false, this);

    for(int i=0; i<SingleQuestionTextCount; ++i)
    {
        m_singleAnswers[i]=new QLineEdit(this);
    }

    for(int i=0; i<MultipleQuestionTextCount; ++i)
    {
        m_multipleAnswers[i]=new QLineEdit(this);
    }

    QGroupBox *metaInfoPanel=new QGroupBox(this);
    metaInfoPanel->setTitle("Infomation");
    QFormLayout *infoLayout=new QFormLayout(metaInfoPanel);
    infoLayout->addRow("UniID", m_metaInfos[TypeUid]);
    infoLayout->addRow("Gender", m_metaInfos[TypeGender]);
    infoLayout->addRow("Age", m_metaInfos[TypeAge]);
    infoLayout->addRow("College", m_metaInfos[TypeCollege]);
    infoLayout->addRow("Know Mondrian", m_metaInfos[TypeKnow]);
    metaInfoPanel->setLayout(infoLayout);

    QGroupBox *singlePanel=new QGroupBox(this);
    singlePanel->setTitle("Single-Choice Answer");
    QBoxLayout *singleLayout=new QBoxLayout(QBoxLayout::TopToBottom, singlePanel);
    singleLayout->addWidget(new QLabel("Which user interface you like most?", this));
    singleLayout->addWidget(m_singleChoices[SingleLike]);
    singleLayout->addWidget(m_singleAnswers[SingleLikeWhy]);
    singleLayout->addWidget(new QLabel("Which user interface helps you make your decision?", this));
    singleLayout->addWidget(m_singleChoices[SingleFast]);
    singleLayout->addWidget(m_singleAnswers[SingleFastWhy]);
    singleLayout->addWidget(new QLabel("Which user interface makes you think it affects your expression?", this));
    singleLayout->addWidget(m_singleChoices[SingleAffect]);
    singleLayout->addWidget(m_singleAnswers[SingleAffectWhy]);
    singlePanel->setLayout(singleLayout);

    QGroupBox *multiplePanel=new QGroupBox(this);
    multiplePanel->setTitle("Multiple-Choice Answer");
    QBoxLayout *multipleLayout=new QBoxLayout(QBoxLayout::TopToBottom, multiplePanel);
    multipleLayout->addWidget(new QLabel("Which user interface you like most?"));
    multipleLayout->addWidget(m_multipleChoices[MultipleLike]);
    multipleLayout->addWidget(m_multipleAnswers[MultipleLikeWhy]);
    multipleLayout->addWidget(new QLabel("Which user interface helps you make your decision?"));
    multipleLayout->addWidget(m_multipleChoices[MultipleHelp]);
    multipleLayout->addWidget(m_multipleAnswers[MultipleHelpWhy]);
    multipleLayout->addWidget(new QLabel("Which user interface slows you down?"));
    multipleLayout->addWidget(m_multipleChoices[MultipleSlow]);
    multipleLayout->addWidget(m_multipleAnswers[MultipleSlowWhy]);
    multipleLayout->addWidget(new QLabel("Which user interface you think is the most complex?"));
    multipleLayout->addWidget(m_multipleChoices[MultipleComplex]);
    multipleLayout->addWidget(m_multipleAnswers[MultipleComplexWhy]);
    multipleLayout->addWidget(new QLabel("Which user interface lets you make your decision faster?"));
    multipleLayout->addWidget(m_multipleChoices[MultipleFaster]);
    multipleLayout->addWidget(new QLabel("Which widget would you prefer for providing response for a single image?"));
    multipleLayout->addWidget(m_multiSelectionWidget);
    multipleLayout->addWidget(m_multipleAnswers[MultipleWidgetWhy]);
    multipleLayout->addWidget(new QLabel("Do you think the rating widget affect your choosing experience?"));
    multipleLayout->addWidget(m_multiSelectionAffect);
    multipleLayout->addWidget(m_multipleAnswers[MultipleWidgetAffectWhy]);
    multiplePanel->setLayout(multipleLayout);

    QBoxLayout *mainLayout=new QBoxLayout(QBoxLayout::TopToBottom, this);
    mainLayout->addWidget(metaInfoPanel);
    mainLayout->addWidget(singlePanel);
    mainLayout->addWidget(multiplePanel);
    mainLayout->addStretch();
    setLayout(mainLayout);
}

void MetadataPanel::setStudent(const StudentInfo &student)
{
    //Save the data.
    m_metaInfos[TypeUid]->setText(student.uid);
    m_metaInfos[TypeGender]->setText(student.gender);
    m_metaInfos[TypeAge]->setText(QString::number(student.age));
    m_metaInfos[TypeCollege]->setText(student.college);
    m_metaInfos[TypeKnow]->setText(student.know);

    //Set the answer for single question.
    const QJsonArray &single=student.single_answer;
    m_singleChoices[SingleLike]->setData(single.at(0).toString());
    m_singleAnswers[SingleLikeWhy]->setText(single.at(1).toString());
    m_singleChoices[SingleFast]->setData(single.at(2).toArray());
    m_singleAnswers[SingleFastWhy]->setText(single.at(3).toString());
    m_singleChoices[SingleAffect]->setData(single.at(4).toArray());
    m_singleAnswers[SingleAffectWhy]->setText(single.at(5).toString());

    for(int i=0; i<SingleQuestionTextCount; ++i)
    {
        m_singleAnswers[i]->setSelection(0, 0);
    }

    //Set the answer for multiple question.
    const QJsonArray &multiple=student.multiple_answer;
    m_multipleChoices[MultipleLike]->setData(multiple.at(0).toString());
    m_multipleAnswers[MultipleLikeWhy]->setText(multiple.at(1).toString());
    m_multipleChoices[MultipleHelp]->setData(multiple.at(2).toArray());
    m_multipleAnswers[MultipleHelpWhy]->setText(multiple.at(3).toString());
    m_multipleChoices[MultipleSlow]->setData(multiple.at(4).toArray());
    m_multipleAnswers[MultipleSlowWhy]->setText(multiple.at(5).toString());
    m_multipleChoices[MultipleComplex]->setData(multiple.at(6).toString());
    m_multipleAnswers[MultipleComplexWhy]->setText(multiple.at(7).toString());
    m_multipleChoices[MultipleFaster]->setData(multiple.at(8).toArray());
    m_multiSelectionWidget->setData(multiple.at(9).toString());
    m_multipleAnswers[MultipleWidgetWhy]->setText(multiple.at(10).toString());
    m_multiSelectionAffect->setData(multiple.at(11).toString());
    m_multipleAnswers[MultipleWidgetAffectWhy]->setText(multiple.at(12).toString());

    for(int i=0; i<MultipleQuestionTextCount; ++i)
    {
        m_multipleAnswers[i]->setSelection(0, 0);
    }
}
