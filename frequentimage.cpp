/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#include <QColor>

#include "frequentimage.h"

#include <QDebug>

#define Radius 8
#define myAbs(a) ((a<0)?(a):(0-a))
#define squ(a) (a*a)

FrequentImage::FrequentImage(QObject *parent) : QObject(parent)
{
    reset();
}

void FrequentImage::reset()
{
    memset(m_currentPoints, 0, sizeof(m_currentPoints));
}

void FrequentImage::addPoints(const QList<QPointF> &pointList)
{
    int powerValue[Radius];
    powerValue[0]=9;
    powerValue[1]=9;
    powerValue[2]=7;
    powerValue[3]=7;
    powerValue[4]=4;
    powerValue[5]=4;
    powerValue[6]=2;
    powerValue[7]=1;
    for(auto i : pointList)
    {
        if(i.x()<10.0 || i.y() < 10.0 || i.x()>1900.0 || i.y() > 1060.0)
        {
            continue;
        }
        int px=(int)i.x(), py=(int)i.y();
        for(int rx=-Radius; rx<=Radius; ++rx)
        {
            for(int ry=-Radius; ry<=Radius; ++ry)
            {
                //Calculate the distance.
                int distancePos=(int)sqrtf((float)(squ(myAbs(rx)) + squ(myAbs(ry))));
                if(distancePos<Radius)
                {
                    m_currentPoints[px+rx][py+ry]+=powerValue[distancePos];
                }
            }
        }
    }
}

void FrequentImage::renderingImage()
{
    //Find the maximum number.
    int max=-1;
    for(int i=10; i<1900; ++i)
    {
        for(int j=10; j<1060; ++j)
        {
            if(m_currentPoints[i][j] > max)
            {
                max=m_currentPoints[i][j];
            }
        }
    }
    qreal max_num = (qreal)max,
            min_draw = max_num * 0.015;
    int p_min_draw=(int)min_draw;
    //Preapre the image.
    m_image=QImage(1920, 1080, QImage::Format_ARGB32);
    m_image.fill(QColor(0,0,0,0));
    //Prepare the calculate for each number.
    for(int i=10; i<1900; ++i)
    {
        for(int j=10; j<1060; ++j)
        {
            //Check the number.
            if(m_currentPoints[i][j]<=p_min_draw)
            {
                continue;
            }
            //For each number, we have to change it to 0-260(Red - Purple).
            qreal colorHue=
                    (qreal)(max-m_currentPoints[i][j]) / max_num * (qreal)260.0;
            QColor pixelColor;
            pixelColor.setHsv(colorHue, 255, 255);
            pixelColor.setAlpha(180);
            m_image.setPixelColor(i, j, pixelColor);
        }
    }
}

QImage FrequentImage::image() const
{
    return m_image;
}
