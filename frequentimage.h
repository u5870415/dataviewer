/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#ifndef FREQUENTIMAGE_H
#define FREQUENTIMAGE_H

#include <QPointF>
#include <QImage>
#include <QList>

#include <QObject>

class FrequentImage : public QObject
{
    Q_OBJECT
public:
    explicit FrequentImage(QObject *parent = 0);
    void reset();

    QImage image() const;

signals:

public slots:
    void addPoints(const QList<QPointF> &pointList);
    void renderingImage();

private:
    QImage m_image;
    int m_currentPoints[1920][1080];
};

#endif // FREQUENTIMAGE_H
