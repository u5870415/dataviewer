#include <QFileDialog>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QSplitter>
#include <QListView>
#include <QTabWidget>
#include <QScrollArea>
#include <QItemSelectionModel>

#include "databaseloader.h"
#include "frequentimagemodel.h"
#include "frequentimagepanel.h"
#include "studentmodel.h"
#include "metadatapanel.h"
#include "stasticpanel.h"
#include "trackpanel.h"
#include "loaddialog.h"

#include "mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    m_loader(new DatabaseLoader()),
    m_dialog(new LoadDialog(this)),
    m_studentModel(new StudentModel(this)),
    m_metadataPanel(new MetadataPanel(this)),
    m_statsticPanel(new StasticPanel(this)),
    m_frequentModel(new FrequentImageModel(this)),
    m_frequentPanel(new FrequentImagePanel(this)),
    m_singleTrackPanel(new TrackPanel(this)),
    m_multipleTrackPanel(new TrackPanel(this))
{
    qRegisterMetaType<StudentInfo>("StudentInfo");
//    conenct(m_loader, &DatabaseLoader::)
    connect(m_loader, &DatabaseLoader::requireAppendData,
            m_studentModel, &StudentModel::append,
            Qt::QueuedConnection);
    connect(this, &MainWindow::requireLoadDatabase,
            m_loader, &DatabaseLoader::loadDatabase,
            Qt::QueuedConnection);
    connect(m_loader, &DatabaseLoader::completeOneTrack,
            m_dialog, &LoadDialog::increaseOne,
            Qt::QueuedConnection);
    connect(m_loader, &DatabaseLoader::allFinished,
            m_dialog, &LoadDialog::hide,
            Qt::QueuedConnection);
    connect(m_loader, &DatabaseLoader::allFinished,
            m_statsticPanel, &StasticPanel::doStatstics,
            Qt::QueuedConnection);
    connect(m_loader, &DatabaseLoader::allFinished,
            m_frequentModel, &FrequentImageModel::renderAll,
            Qt::QueuedConnection);
    connect(m_loader, &DatabaseLoader::requireAppendMap,
            m_frequentModel, &FrequentImageModel::addPoints,
            Qt::QueuedConnection);
    m_loader->moveToThread(&m_loaderThread);
    m_loaderThread.start();

    m_statsticPanel->setModel(m_studentModel);
    m_frequentPanel->setModel(m_frequentModel);

    QMenu *fileMenu=new QMenu("&File", this);
    menuBar()->addMenu(fileMenu);
    QAction *open=new QAction("&Open database...", this);
    open->setShortcut(QKeySequence(QKeySequence::Open));
    connect(open, &QAction::triggered,
            [=]
    {
        QString databasePath=QFileDialog::getOpenFileName();
        if(databasePath.isEmpty())
        {
            return;
        }
        //Clear the model.
        m_studentModel->reset();
        //Show the dialog.
        m_dialog->show();
        emit requireLoadDatabase(databasePath);
    });
    fileMenu->addAction(open);

    QMenu *viewMenu=new QMenu("&View", this);
    menuBar()->addMenu(viewMenu);
    QAction *showStatistic=new QAction("Show &Stastics");
    connect(showStatistic, &QAction::triggered,
            m_statsticPanel, &StasticPanel::showResult);
    viewMenu->addAction(showStatistic);
    QAction *showLatex=new QAction("Show &Latex");
    connect(showLatex, &QAction::triggered,
            m_statsticPanel, &StasticPanel::showLatex);
    viewMenu->addAction(showLatex);

    QList<QPixmap> singleBackgrounds;
    singleBackgrounds << QPixmap("://images/Single_Yes.png")
                      << QPixmap("://images/Single_Yes_No.png")
                      << QPixmap("://images/Single_Radio.png")
                      << QPixmap("://images/Single_Heart.png")
                      << QPixmap("://images/Single_Slider.png");
    m_singleTrackPanel->setBackgrounds(singleBackgrounds);

    QList<int> tracks;
    tracks.clear();
    tracks << 0 << 6 << 7 << 1 << 5 << 3 << 4 << 2;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 5 << 7 << 6 << 2 << 1 << 0 << 3 << 4;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 3 << 4 << 5 << 6 << 0 << 2 << 1 << 7;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 4 << 3 << 0 << 5 << 2 << 6 << 7 << 1;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 1 << 0 << 3 << 7 << 4 << 5 << 2 << 6;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 7 << 5 << 2 << 4 << 3 << 1 << 6 << 0;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 2 << 1 << 4 << 0 << 6 << 7 << 5 << 3;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 6 << 2 << 1 << 3 << 7 << 4 << 0 << 5;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 0 << 7 << 1 << 5 << 4 << 2 << 3 << 6;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 3 << 0 << 5 << 1 << 7 << 6 << 4 << 2;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 5 << 1 << 2 << 0 << 3 << 7 << 6 << 4;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 4 << 5 << 3 << 7 << 6 << 1 << 2 << 0;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 2 << 6 << 0 << 3 << 5 << 4 << 7 << 1;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 6 << 4 << 7 << 2 << 0 << 3 << 1 << 5;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 1 << 3 << 4 << 6 << 2 << 5 << 0 << 7;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 7 << 2 << 6 << 4 << 1 << 0 << 5 << 3;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 7 << 3 << 6 << 0 << 2 << 5 << 4 << 1;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 2 << 7 << 1 << 3 << 6 << 0 << 5 << 4;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 4 << 6 << 7 << 2 << 5 << 1 << 0 << 3;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 5 << 2 << 0 << 4 << 3 << 7 << 1 << 6;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 1 << 5 << 2 << 7 << 4 << 6 << 3 << 0;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 6 << 0 << 3 << 5 << 1 << 4 << 7 << 2;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 3 << 4 << 5 << 1 << 0 << 2 << 6 << 7;
    m_multipleTracks.append(tracks);
    tracks.clear();
    tracks << 0 << 1 << 4 << 6 << 7 << 3 << 2 << 5;
    m_multipleTracks.append(tracks);

    QList<QPixmap> multipleBackgrounds;
    multipleBackgrounds << QPixmap("://images/Multiple_list.png")
                        << QPixmap("://images/Multiple_grid.png")
                        << QPixmap("://images/Multiple_drive.png")
                        << QPixmap("://images/Multiple_double.png");
    m_multipleTrackPanel->setBackgrounds(multipleBackgrounds);

    QSplitter *container=new QSplitter(this);
    container->setContentsMargins(5, 5, 5, 5);
    QListView *studentView=new QListView(this);
    studentView->setModel(m_studentModel);
    connect(studentView->selectionModel(),
            &QItemSelectionModel::currentChanged,
            [=](const QModelIndex index)
    {
        const StudentInfo &student=
                m_studentModel->studentInfo(index);
        m_metadataPanel->setStudent(student);

        m_singleTrackPanel->setTracks(student.single_duration,
                                      durationList(student.single_duration),
                                      student.single_tracks,
                                      student.single_types);
        m_multipleTrackPanel->setTracks(
                    durationsTotal(student.multiple_first_duration,
                                   student.multiple_second_duration),
                    durationList(student.multiple_first_duration,
                                 student.multiple_second_duration),
                    student.multiple_tracks,
                    m_multipleTracks.at(student.row.toInt()));
    });
    container->addWidget(studentView);
    setCentralWidget(container);

    QTabWidget *tabbedWidget=new QTabWidget(this);
    QScrollArea *metadataArea=new QScrollArea(this);
    metadataArea->setWidgetResizable(true);
    metadataArea->setWidget(m_metadataPanel);
    tabbedWidget->addTab(metadataArea, "Metadata");
    tabbedWidget->addTab(m_singleTrackPanel, "Single Track");
    tabbedWidget->addTab(m_multipleTrackPanel, "Multiple Track");
    tabbedWidget->addTab(m_statsticPanel, "Statstic");
    tabbedWidget->addTab(m_frequentPanel, "Heat Maps");
    container->addWidget(tabbedWidget);
    container->setStretchFactor(1, 1);
}

MainWindow::~MainWindow()
{
    m_loaderThread.quit();
    m_loaderThread.wait();
    m_loader->deleteLater();
}

QList<qint64> MainWindow::durationsTotal(const QList<qint64> &duration_first,
                                         const QList<qint64> &duration_second)
{
    QList<qint64> durations;
    for(int i=0; i<duration_first.size(); ++i)
    {
        durations.append(duration_first.at(i) + duration_second.at(i));
    }
    return durations;
}

QStringList MainWindow::durationList(const QList<qint64> &durations)
{
    QStringList durationText;
    for(auto i:durations)
    {
        durationText.append(QString::number(i) + " ms");
    }
    return durationText;
}

QStringList MainWindow::durationList(const QList<qint64> &duration_first,
                                     const QList<qint64> &duration_second)
{
    QStringList durationText;
    for(int i=0; i<duration_first.size(); ++i)
    {
        durationText.append(QString::number(duration_first.at(i)) + " ms - " +
                            QString::number(duration_second.at(i)) + " ms");
    }
    return durationText;
}
