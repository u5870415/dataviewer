#ifndef DATABASELOADER_H
#define DATABASELOADER_H

#include <QStringList>
#include <QSqlDatabase>

#include "util.h"

#include <QObject>

using namespace Util;

class StudentModel;
class DatabaseLoader : public QObject
{
    Q_OBJECT
public:
    explicit DatabaseLoader(QObject *parent = 0);
    ~DatabaseLoader();
    bool loadDatabase(const QString &fileName);

signals:
    void completeOneTrack();
    void allFinished();
    void requireAppendData(StudentInfo student);
    void requireAppendMap(int index, QList<QPointF> points);

public slots:

private:
    void loadStudent(const QString &infoRowName);
    QSqlDatabase m_database;
    QStringList m_userList;
};

#endif // DATABASELOADER_H
