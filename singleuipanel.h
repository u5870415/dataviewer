#ifndef SINGLEUIPANEL_H
#define SINGLEUIPANEL_H

#include <QJsonArray>

#include <QWidget>

class QAbstractButton;
class SingleUiPanel : public QWidget
{
    Q_OBJECT
public:
    explicit SingleUiPanel(const QStringList &items,
                           bool isMultiple, QWidget *parent = 0);
    void setData(const QString &singleChoice);
    void setData(const QJsonArray &multipleChoice);

signals:

public slots:

private:
    QList<QAbstractButton *> m_selections;
};

#endif // SINGLEUIPANEL_H
