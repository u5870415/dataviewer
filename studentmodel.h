#ifndef STUDENTMODEL_H
#define STUDENTMODEL_H

#include "util.h"

#include <QAbstractListModel>

using namespace Util;

class StudentModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit StudentModel(QObject *parent = 0);
    int rowCount(const QModelIndex &parent=QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const;
    void append(const StudentInfo &data);
    void reset();
    StudentInfo studentInfo(int row);
    StudentInfo studentInfo(const QModelIndex &index);

signals:

public slots:

private:
    QList<StudentInfo> m_studentList;
};

#endif // STUDENTMODEL_H
