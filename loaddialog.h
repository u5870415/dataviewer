#ifndef LOADDIALOG_H
#define LOADDIALOG_H

#include <QDialog>

class QProgressBar;
class LoadDialog : public QDialog
{
    Q_OBJECT
public:
    explicit LoadDialog(QWidget *parent = 0);

signals:

public slots:
    void increaseOne();

protected:
    void showEvent(QShowEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;

private:
    QProgressBar *m_progress;
};

#endif // LOADDIALOG_H
