/*
 * Copyright (C) Kreogist Dev Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
#include <QPainter>

#include "doublelayerviewer.h"

DoubleLayerViewer::DoubleLayerViewer(QWidget *parent) : QWidget(parent),
    m_imageX(0), m_imageY(0)
{

}

QPixmap DoubleLayerViewer::foreground() const
{
    return m_foreground;
}

void DoubleLayerViewer::setForeground(const QPixmap &foreground)
{
    m_foreground = foreground;
    updateForeground();
}

QPixmap DoubleLayerViewer::background() const
{
    return m_background;
}

void DoubleLayerViewer::setBackground(const QPixmap &background)
{
    m_background = background;
    updateBackground();
}

void DoubleLayerViewer::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing |
                           QPainter::TextAntialiasing |
                           QPainter::SmoothPixmapTransform, true);
    if(!m_scaledBackground.isNull())
    {
        painter.drawPixmap(m_imageX, m_imageY, m_scaledBackground);
    }
    if(!m_scaledForeground.isNull())
    {
        painter.drawPixmap(m_imageX, m_imageY, m_scaledForeground);
    }
}

void DoubleLayerViewer::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    //Update the image.
    updateForeground();
    updateBackground();
}

void DoubleLayerViewer::updateForeground()
{
    if(!m_foreground.isNull())
    {
        m_scaledForeground=m_foreground.scaled(size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    }
    update();
}

void DoubleLayerViewer::updateBackground()
{
    if(!m_background.isNull())
    {
        m_scaledBackground=m_background.scaled(size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
        m_imageX=(width()-m_scaledBackground.width())>>1;
        m_imageY=(height()-m_scaledBackground.height())>>1;
    };
    update();
}
