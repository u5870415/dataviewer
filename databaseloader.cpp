#include <QSqlQuery>
#include <QSqlError>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "databaseloader.h"
#include "util.h"

#include <QDebug>

DatabaseLoader::DatabaseLoader(QObject *parent) : QObject(parent),
    m_database(QSqlDatabase::addDatabase("QSQLITE"))
{
}

DatabaseLoader::~DatabaseLoader()
{
    m_database.close();
}

bool DatabaseLoader::loadDatabase(const QString &fileName)
{
    if(m_database.isOpen())
    {
        m_database.close();
    }
    m_database.setDatabaseName(fileName);
    if(!m_database.open())
    {
        return false;
    }
    QSqlQuery query("select title from selection_expresult");
    QStringList titleList;
    while(query.next())
    {
        titleList.append(query.value(0).toString());
    }
    //Get all the uni ID.
    for(auto i : titleList)
    {
        if(i.contains("info-row-"))
        {
            loadStudent(i);
        }
    }
    emit allFinished();
    return true;
}

void DatabaseLoader::loadStudent(const QString &infoRowName)
{
    //Get student information.
    QString uniId=infoRowName.left(8),
            rowId=infoRowName.mid(infoRowName.lastIndexOf('-')+1);
    //Load answers.
    QString metaTitle=uniId + "|info-row-" + rowId;
    QSqlQuery query;
    //Prepare student.
    StudentInfo student;
    //Set data.
    student.uid=uniId;
    student.row=rowId;
    //Metadata.
    query.prepare("select title, result from selection_expresult where title='"
                  +metaTitle+"'");
    query.exec();
    if(query.next())
    {
        //Get the result.
        QJsonArray metaInfo=QJsonDocument::fromJson(
                    query.value(1).toString().toLocal8Bit()).array();
        //Save the info.
        student.gender=metaInfo.at(1).toString();
        student.age=metaInfo.at(2).toInt();
        student.college=metaInfo.at(3).toString();
        student.know=metaInfo.at(4).toString();
    }
    //Load the single experiment result.
    for(int single_id = 0; single_id < 27; ++single_id)
    {
        QString singleTitle=uniId + "|iteration-initial-%1-eyetribe-row-" +
                            rowId;
        query.prepare("select title, result from selection_expresult where "
                      "title='" + singleTitle.arg(QString::number(single_id)) +
                      "'");
        query.exec();
        if(query.next())
        {
            //Get the result.
            QJsonArray singleTribe=
                    QJsonDocument::fromJson(
                        query.value(1).toString().toLocal8Bit()).object().value(
                        "eyetribe").toArray();
            //Get the frame size.
            int frameSize=singleTribe.size();
            //Prepare the frame list.
            QList<QPointF> track;
            //Parse all frame.
            for(int i=0; i<frameSize; ++i)
            {
                //Translate item.
                QJsonObject frameObject=
                        QJsonDocument::fromJson(
                            singleTribe.at(i).toString().toLocal8Bit()).object();
                //Get the avg data.
                frameObject = frameObject.value("avg").toObject();
                // Translate it into a point.
                QPointF framePosition(frameObject.value("x").toDouble(),
                                      frameObject.value("y").toDouble());
                track.append(framePosition);
            }
            //Append the track to list.
            student.single_tracks.append(track);
            emit completeOneTrack();
        }
        else
        {
            QList<QPointF> track;
            student.single_tracks.append(track);
        }
    }
    //Load the single test result.
    QString singleType =uniId+"|iteration-initial-row-"+rowId;
    query.prepare("select title, result from selection_expresult where title='"
                  +singleType+"'");
    query.exec();
    if(query.next())
    {
        //Get the result.
        QJsonArray singleResult=QJsonDocument::fromJson(
                    query.value(1).toString().toLocal8Bit()).array();
        QList<int> singleType;
        QList<qint64> singleDuration;
        for(auto i : singleResult)
        {
            QJsonObject singleItem=i.toObject();
            int uiType=singleItem.value("ui").toInt();
            //Append the type.
            if(singleType.size() < student.single_tracks.size())
            {
                emit requireAppendMap(uiType, student.single_tracks.at(singleType.size()));
            }
            singleType.append(uiType);
            singleDuration.append((qint64)singleItem.value("duration").toDouble());
        }
        student.single_types=singleType;
        student.single_duration=singleDuration;
    }
    //Single questions.
    QString singleQuestion=uniId+"|single-question-row-"+rowId;
    query.prepare("select title, result from selection_expresult where title='"
                  +singleQuestion+"'");
    query.exec();
    if(query.next())
    {
        //Get the result.
        student.single_answer=QJsonDocument::fromJson(
                    query.value(1).toString().toLocal8Bit()).array();
    }
    QList<qint64> multipleFirstDuration, multipleSecondDuration;
    //Load the single experiment result.
    for(int multiple_id = 0; multiple_id < 8; ++multiple_id)
    {
        QString multipleTitle=uniId + "|iteration-%1-eyetribe-row-" +
                            rowId;
        query.prepare("select title, result from selection_expresult where "
                      "title='" + multipleTitle.arg(QString::number(multiple_id)) +
                      "'");
        query.exec();
        if(query.next())
        {
            //Get the result.
            QJsonArray singleTribe=
                    QJsonDocument::fromJson(
                        query.value(1).toString().toLocal8Bit()).object().value(
                        "eyetribe").toArray();
            //Get the frame size.
            int frameSize=singleTribe.size();
            //Prepare the frame list.
            QList<QPointF> track;
            //Parse all frame.
            for(int i=0; i<frameSize; ++i)
            {
                //Translate item.
                QJsonObject frameObject=
                        QJsonDocument::fromJson(
                            singleTribe.at(i).toString().toLocal8Bit()).object();
                //Get the avg data.
                frameObject = frameObject.value("avg").toObject();
                // Translate it into a point.
                QPointF framePosition(frameObject.value("x").toDouble(),
                                      frameObject.value("y").toDouble());
                track.append(framePosition);
            }
            //Append the track to list.
            student.multiple_tracks.append(track);
            emit completeOneTrack();
        }

        QString multipleResultTitle=uniId + "|iteration-%1-row-" + rowId;
        query.prepare("select title, result from selection_expresult where "
                      "title='" + multipleResultTitle.arg(QString::number(multiple_id)) +
                      "'");
        query.exec();
        if(query.next())
        {
            //Get the result.
            QJsonObject multipleData = QJsonDocument::fromJson(
                        query.value(1).toString().toLocal8Bit()).object();
            multipleFirstDuration.append(multipleData.value("duration-first").toInt());
            multipleSecondDuration.append(multipleData.value("duration-second").toInt());
        }
    }
    student.multiple_first_duration = multipleFirstDuration;
    student.multiple_second_duration = multipleSecondDuration;
    //Multiple questions.
    QString multipleQuestion=uniId+"|multiple-question-row-"+rowId;
    query.prepare("select title, result from selection_expresult where title='"
                  ""+multipleQuestion+"'");
    query.exec();
    while(query.next())
    {
        student.multiple_answer=QJsonDocument::fromJson(
                    query.value(1).toString().toLocal8Bit()).array();
    }
    //Add student.
    emit requireAppendData(student);
}
