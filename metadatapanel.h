#ifndef METADATAPANEL_H
#define METADATAPANEL_H

#include <QJsonValue>

#include "util.h"

#include <QWidget>

using namespace Util;

class QLineEdit;
class SingleUiPanel;
class MetadataPanel : public QWidget
{
    Q_OBJECT
public:
    explicit MetadataPanel(QWidget *parent = 0);

signals:

public slots:
    void setStudent(const StudentInfo &student);

private:
    QString arrayToSingle(const QJsonValue &value);
    QString m_singleUiNames[SingleUiCount];
    SingleUiPanel *m_singleChoices[SingleQuestionCount],
                  *m_multipleChoices[MultipleQuestionCount],
                  *m_multiSelectionWidget,
                  *m_multiSelectionAffect;
    QLineEdit *m_metaInfos[MetadataInfoCount],
              *m_singleAnswers[SingleQuestionTextCount],
              *m_multipleAnswers[MultipleQuestionTextCount];
};

#endif // METADATAPANEL_H
