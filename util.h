#ifndef UTIL_H
#define UTIL_H

#include <QPointF>
#include <QString>
#include <QList>
#include <QJsonArray>

namespace Util
{
enum MetadataInfoType
{
    TypeUid,
    TypeGender,
    TypeAge,
    TypeCollege,
    TypeKnow,
    MetadataInfoCount
};

enum SingleUIName
{
    SingleYesButton,
    SingleYesNoButton,
    SingleRadio,
    SingleHeart,
    SingleSlider,
    SingleUiCount
};

enum MultipleUIName
{
    MultipleList,
    MultipleGrid,
    MultipleDrive,
    MultipleTwo,
    MultipleUiCount
};

enum SingleQuestionType
{
    SingleLike,
    SingleFast,
    SingleAffect,
    SingleQuestionCount
};

enum MultipleQuestionType
{
    MultipleLike,
    MultipleHelp,
    MultipleSlow,
    MultipleComplex,
    MultipleFaster,
    MultipleQuestionCount
};

enum MultipleQuestionTextType
{
    MultipleLikeWhy,
    MultipleHelpWhy,
    MultipleSlowWhy,
    MultipleComplexWhy,
    MultipleWidgetWhy,
    MultipleWidgetAffectWhy,
    MultipleQuestionTextCount
};

enum SingleQuestionTextType
{
    SingleLikeWhy,
    SingleFastWhy,
    SingleAffectWhy,
    SingleQuestionTextCount
};

struct StudentInfo
{
    QString uid;
    QString row;
    QString gender;
    int age;
    QString college;
    QString know;
    QList<int> single_types;
    QList<qint64> single_duration;
    QList<QList<QPointF>> single_tracks;
    QList<qint64> multiple_first_duration;
    QList<qint64> multiple_second_duration;
    QList<QList<QPointF>> multiple_tracks;
    QJsonArray single_answer;
    QJsonArray multiple_answer;
};
}

#endif // UTIL_H
