#include <QProgressBar>
#include <QBoxLayout>

#include "loaddialog.h"

#include <QDebug>

LoadDialog::LoadDialog(QWidget *parent) : QDialog(parent),
    m_progress(new QProgressBar(this))
{
    setWindowTitle("Loading Database");
    setFixedSize(200, 50);
    setWindowFlags(windowFlags() & ~Qt::WindowCloseButtonHint & ~Qt::WindowContextHelpButtonHint);
    m_progress->setMaximum(792);
}

void LoadDialog::increaseOne()
{
    m_progress->setValue(m_progress->value()+1);
}

void LoadDialog::showEvent(QShowEvent *event)
{
    //SHow.
    QDialog::showEvent(event);
    m_progress->setValue(0);
}

void LoadDialog::resizeEvent(QResizeEvent *event)
{
    QDialog::resizeEvent(event);
    m_progress->setGeometry(5, 5, width()-10, height()-10);
}
